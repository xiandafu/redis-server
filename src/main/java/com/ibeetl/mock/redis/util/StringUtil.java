package com.ibeetl.mock.redis.util;



import com.ibeetl.mock.redis.protocal.operation.ListOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public abstract class StringUtil {

	static Log logger = LogFactory.getLog(StringUtil.class);

    public static void printCharBuffer(ByteBuffer bf) {
        if (bf.remaining() > 0) {
            logger.debug(new String(bf.array(), bf.position(), bf.remaining(), StandardCharsets.UTF_8));
        }
    }

    public static String redisErrorMessage(Exception ex) {
        String message = ex.getMessage();
        if (message != null) {
            message = message.replaceAll("\\r", "");
        }
        return message;
    }
}
