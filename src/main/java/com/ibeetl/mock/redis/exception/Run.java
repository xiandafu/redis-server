package com.ibeetl.mock.redis.exception;


import org.apache.commons.logging.Log;

@FunctionalInterface
public interface Run {
    void run() throws Exception;

    static void ignoreException(Log logger, Run warnLogger) {
        try {
            warnLogger.run();
        } catch (Exception ex) {
            logger.warn("Sever Exception : "+ ex.getMessage());
        }
    }

    static void silentException(Run warnLogger) {
        try {
            warnLogger.run();
        } catch (Exception ex) {
        }
    }
}
