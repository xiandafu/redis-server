package com.ibeetl.mock.redis.protocal.jedis;

public interface ProtocolCommand {

    byte[] getRaw();

}
