package com.ibeetl.mock.redis.protocal.operation;

import com.ibeetl.mock.redis.protocal.AbstractOperation;
import com.ibeetl.mock.redis.protocal.RedisRequest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

public class ClusterOperation extends AbstractOperation  {
	String clusterSlot = "*1\r\n" + "*3\r\n" + ":0\r\n" + ":16383\r\n" + "*3\r\n"
			+ "+host\r\n" + ":port\r\n" + "+821d8ca00d7ccf931ed3ffc7e3db0599d2271abf\r\n";
	public void cluster(RedisRequest request) throws IOException {

		InetSocketAddress address = (InetSocketAddress) request.getServer().getServerSocket().getLocalSocketAddress();
		String url = clusterSlot.replace("host",address.getHostName()).replace("port",String.valueOf(address.getPort()));
		request.getOutputStream().write(url.getBytes(StandardCharsets.UTF_8));
	}
}
