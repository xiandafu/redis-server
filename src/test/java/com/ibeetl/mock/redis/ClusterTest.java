package com.ibeetl.mock.redis;

import com.ibeetl.mock.redis.protocal.AbstractOperation;
import com.ibeetl.mock.redis.protocal.RedisRequest;
import com.ibeetl.mock.redis.protocal.operation.Server;
import org.junit.*;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ClusterTest {
//	@Test
//	public void test() throws IOException {
//
//		JedisCluster jedis = connection();
//		String key1 = UUID.randomUUID().toString();
//		jedis.set(key1, key1);
//		String value1 = jedis.get(key1);
//		Assert.assertEquals(key1,value1);
//		jedis.set(key1, "abc");
//		String value2 = jedis.get(key1);
//		Assert.assertEquals("abc",value2);
//		jedis.close();
//	}

	RedisServer server;


	protected JedisCluster connection() throws IOException {
		InetSocketAddress address = (InetSocketAddress)server.getServerSocket().getLocalSocketAddress();


		Set<HostAndPort> hostAndPortsSet = new HashSet<HostAndPort>();
		// 添加节点
		hostAndPortsSet.add(new HostAndPort(address.getHostName(), address.getPort()));


		// Jedis连接池配置
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

		//最小空闲连接数, 默认0
		jedisPoolConfig.setMinIdle(0);
		// 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
		jedisPoolConfig.setMaxWaitMillis(200000); // 设置2秒
		//对拿到的connection进行validateObject校验
		jedisPoolConfig.setTestOnBorrow(true);


		JedisCluster jedis = new JedisCluster(hostAndPortsSet,jedisPoolConfig);
		return jedis;
		// return new Jedis("192.168.1.246", 6379, 60_000);
	}


	@Test
	public void testPing() throws IOException {
		JedisCluster jedis = connection();
		jedis.set("abc","efg");
		jedis.set("abc","efg");
		jedis.set("abc","efg");
		jedis.set("abc","efg");
		String value = jedis.get("abc");
		Assert.assertEquals("efg",value);
		String key1 = UUID.randomUUID().toString();
		jedis.set(key1, key1);
		String value1 = jedis.get(key1);
		Assert.assertEquals(key1,value1);
		jedis.set(key1, "abc");
		String value2 = jedis.get(key1);
		Assert.assertEquals("abc",value2);

	}


	@Test
	public void testPing2() throws IOException {
		JedisCluster jedis = connection();
		jedis.set("abc","efg");
		jedis.set("abc","efg");
		jedis.set("abc","efg");
		jedis.set("abc","efg");
		String value = jedis.get("abc");
		Assert.assertEquals("efg",value);

	}


	@Before
	public void init() throws IOException {
		server = new RedisServer();
		server.listener("localhost",9999);
	}

	@After
	public void close() throws IOException {
		server.clean();
	}



}
