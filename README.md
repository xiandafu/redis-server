# redis-server

## 介绍
用来单元测试，模拟redis-server，代码参考https://github.com/microwww/redis-mock，考虑到使用，先复制了一份，之后会PR给microwww/redis-mock

安装
```xml
<dependency>
	<groupId>com.ibeetl</groupId>
	<artifactId>redis-server</artifactId>
	<version>0.1.0</version>
</dependency>

```

示例

```
public class RedisServerTest {


    RedisServer server;
    @Before
    public void init() throws IOException {
        server = new RedisServer();
        server.listener("localhost",9999);
    }

    @After
    public void close() throws IOException {
        server.close();
    }


    @Test
    public void testPing2() throws IOException {
        JedisCluster jedis = connection();
        jedis.set("abc","efg");
   
        String value = jedis.get("abc");
        Assert.assertEquals("efg",value);

    }

  protected JedisCluster connection() throws IOException {
        InetSocketAddress address = (InetSocketAddress)server.getServerSocket().getLocalSocketAddress();


        Set<HostAndPort> hostAndPortsSet = new HashSet<HostAndPort>();
        // 添加节点
        hostAndPortsSet.add(new HostAndPort(address.getHostName(), address.getPort()));


        // Jedis连接池配置
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

        //最小空闲连接数, 默认0
        jedisPoolConfig.setMinIdle(0);
        // 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
        jedisPoolConfig.setMaxWaitMillis(200000); // 设置2秒
        //对拿到的connection进行validateObject校验
        jedisPoolConfig.setTestOnBorrow(true);


        JedisCluster jedis = new JedisCluster(hostAndPortsSet,jedisPoolConfig);
        return jedis;
    }



}

```


# 支持的命令

## Cluster
CLUSTER SLOTS

## ConnectionOperation

AUTH, ECHO, PING, QUIT, SELECT,

## HashOperation

HDEL, HEXISTS, HGET, HGETALL, HINCRBY, HINCRBYFLOAT, HKEYS, HLEN, HMGET, HMSET, HSCAN, HSET, HSETNX, HVALS,

## KeyOperation

DEL, EXISTS, EXPIRE, EXPIREAT, KEYS, MOVE, PERSIST, PEXPIRE, PEXPIREAT, PTTL, RANDOMKEY, RENAME, RENAMENX, SCAN, SORT, TTL, TYPE, UNLINK<4.0.0+>,

## ListOperation

BLPOP, BRPOP, LINDEX, LINSERT, LLEN, LPOP, LPUSH, LPUSHX, LRANGE, LREM, LSET, LTRIM, RPOP, RPOPLPUSH, RPUSH, RPUSHX,



## ServerOperation

DBSIZE, FLUSHALL<ASYNC, 4.0.0+>, FLUSHDB<ASYNC, 4.0.0+>, TIME, 0.0.2+, CLIENT GETNAME, CLIENT KILL, CLIENT LIST, CLIENT SETNAME,

## SetOperation

SADD, SCARD, SDIFF, SDIFFSTORE, SINTER, SINTERSTORE, SISMEMBER, SMEMBERS, SMOVE, SPOP, SRANDMEMBER, SREM, SSCAN, SUNION, SUNIONSTORE,

## SortedSetOperation

ZADD, ZCARD, ZCOUNT, ZINCRBY, ZINTERSTORE, ZRANGE, ZRANGEBYSCORE, ZRANK, ZREM, ZREMRANGEBYRANK, ZREMRANGEBYSCORE, ZREVRANGE, ZREVRANGEBYSCORE, ZREVRANK, ZSCAN, ZSCORE, ZUNIONSTORE,

## StringOperation

APPEND, BITCOUNT, BITOP, DECR, DECRBY, GET, GETBIT, GETRANGE, GETSET, INCR, INCRBY, INCRBYFLOAT, MGET, MSET, MSETNX, PSETEX, SET, SETBIT, SETEX, SETNX, SETRANGE, STRLEN,

## TransactionOperation

0.0.2+, DISCARD, EXEC, MULTI, UNWATCH, WATCH

## 自定义

如果发现有不支持得Redis命令，可以自己添加
server.configScheme(16, new YourOperation1(), ...)

实现redis协议 https://www.redis.com.cn/topics/protocol.html


